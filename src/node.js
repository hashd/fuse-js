import _ from 'lodash'

export default class Node {
  constructor(name = 'set', attributes, childNodes) {
    this.name = name
    this.attributes = {}
    this.childNodes = []

    if (attributes !== undefined) {
      _.forOwn(attributes, (v, k) => this.setAttribute(k, v))
    }

    this.childNodes.forEach(d => this.childNodes.push(d))
  }

  setAttribute(key, value) {
    this.attributes[key] = value
    return this
  }

  setAttributes(key, value) {
    if (value !== undefined) {
      this.setAttribute(key, value)
    } else if (key instanceof Object) {
      _.forOwn(key, (v, k) => this.setAttribute(k, v))
    }

    return this
  }

  addNode(node) {
    if (node instanceof Node) {
      this.childNodes.push(node)
    } else if (node instanceof Array) {
      node.forEach(d => this.childNodes.push(d))
    }

    return this
  }

  addNodes(name, specificAttributes, defaultAttributes) {
    console.log(`${name} -- ${specificAttributes} -- ${defaultAttributes}`)
    specificAttributes.forEach((attributes) =>
      this.addNode((new Node(name)).setAttributes(defaultAttributes).setAttributes(attributes))
    )
    return this
  }

  addVarNodes(...nodes) {
    nodes.forEach(d => this.addNode(d))
    return this
  }

  toXml(level = 0) {
    let chartXml = ''
    let indentationString = ''
    for (let i = 0; i < level; i++) {
      indentationString += '  '
    }
    level++

    chartXml += `${indentationString}<${this.name}${Utils.getPropertyString(this.attributes)}>${this.childNodes.length===0?"":"\n"}`
    this.childNodes.forEach(node => 
      chartXml += node.toXml(level)
    )
    chartXml += `</${this.name}>\n`

    return chartXml
  }
}
