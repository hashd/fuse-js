export class Utils {
  static addProperty(node, key, value) {
    if (value !== undefined) {
      node.attributes[key] = value
    } else if (key instanceof Object) {
      _.forOwn(key, (v, k) => node.setAttribute(k, v))
    }
  }

  static getPropertyString(attributes) {
    let propertyString = ''
    _.forOwn(attributes, (v, k) => propertyString += ` ${k}="${v}"`)
    return propertyString
  }

  static getPascalCasedName(name) {
    return name.substring(0,1).toUpperCase() + name.substring()
  }

  /*static getPropertyMap(propertyString) {
    let propertyMap = {}
    $(propertyString).each(function () {
      let tagName = this.tagName
      
      propertyMap[tagName] = {}
      this.attributes.forEach(d => propertyMap[tagName][d.name] = d.value)
      return propertyMap
    })
  }*/
}