export class Category extends Node {
  constructor(attributes, childnodes) {
    super('category', attributes, childnodes)
  }

  setLabel(value) {
    return this.setAttribute('label', value)
  }

  setToolText(value) {
    return this.setAttribute('toolText', value)
  }

  setShowLabel(value = true) {
    return this.setAttribute('showLabel', value)
  }
}

export class Categories extends Node {
  constructor(attributes, childnodes) {
    super('categories', attributes, childnodes)
  }

  setFontColor(value) {
    return this.setAttribute('fontColor', value)
  }

  setFontSize(value) {
    return this.setAttribute('fontSize', value)
  }

  setFont(value) {
    return this.setAttribute('font', value)
  }
}

export class Dataset extends Node {
  constructor(attributes, childnodes) {
    super('dataset', attributes, childnodes)
  }

  setSeriesName(value) {
    return this.setAttribute('seriesName', value)
  }

  setShowValues(value = true) {
    return this.setAttribute('showValues', value)
  }

  setColor(value) {
    return this.setAttribute('color', value)
  }

  setIncludeInLegend(value = true) {
    return this.setAttribute('includeInLegend', value)
  }

  setAlpha(value) {
    return this.setAttribute('alpha', value)
  }
}