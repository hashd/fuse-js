var 
  gulp     = require('gulp'),
  babel    = require('gulp-babel'),
  concat   = require('gulp-concat'),
  mocha    = require('gulp-mocha');

gulp.task('build', function () {
  return gulp.src("src/*.js")
    .pipe(concat('fuse-js.js'))
    //.pipe(eslint())
    //.pipe(eslint.format())
    .pipe(babel())
    .pipe(gulp.dest('build'));
});

gulp.task('test', function () {
  return gulp.src('test/*.js', {read: false})
    .pipe(mocha({reporter: 'nyan'}));
});

gulp.task('default', ['build', 'test']);