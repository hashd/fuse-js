"use strict";
var assert = require("assert"),
  path = require("path"),
  _ = require('lodash'),
  chai = require('chai'),
  node = require(path.join(__dirname, '../build', 'fuse-js')),
  Node = node.default;

chai.should()

describe('Node', function() {
  describe('Functionality', function () {
    it('should create a node of name: set using Node class', function () {
      assert(new Node() instanceof Node);
      assert.equal((new Node()).name, 'set');
    });

    it('should have attributes when passed as an argument', function () {
      let setNode = new Node(undefined, {showValues: true, marginLeft: 10, marginRight: 10, caption: "Values of heaven"});
      
      setNode.should.have.property('attributes');
      setNode.attributes.should.have.keys(['showValues', 'marginLeft', 'marginRight', 'caption']);

      assert.equal(setNode.attributes['showValues'], true);
      assert.equal(setNode.attributes['marginLeft'], 10);
      assert.equal(setNode.attributes['marginRight'], 10);
      assert.equal(setNode.attributes['caption'], 'Values of heaven');
    });
  });
});